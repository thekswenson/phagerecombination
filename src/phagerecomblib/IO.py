# Copyright 2019 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
IO functions for phage recombination files.
"""
from typing import Dict
from typing import TextIO

from .Phage import Phage

def printCommunityToFile(cname: str, c: Dict[str, Phage], filehandle: TextIO):
  """
  Output a phage communty.
  """
  filehandle.write(f'Community Name: {cname}\n')
  for name, p in c.items():
    filehandle.write(f'{name:>3}: {"".join(p)}\n')

