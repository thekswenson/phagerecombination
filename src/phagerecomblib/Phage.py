# Copyright 2019 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
The interval graph classes.
"""
import sys
import re
from collections import defaultdict as defdict

from typing import List, overload
from typing import Dict
from typing import Tuple
from typing import Union
from typing import Optional
from typing import TextIO


COMMENT_CHAR = '#'    #Comment character for lines in input file.
NEWP1 = 'N'           #The prefix for a new parent.
NEWP2 = 'M'           #The prefix for the other new parent.

#GAP_CHAR = '-'       #Gaps character representing an empty variant.
NO_MATCH = '_'        #Represents a parent variant that matches nothing.
WILDCARD = '*'        #Represents a child variant that matches anything.
EMPTY_CHAR = '.'      #The character to print for element outsize an interval.
DUMMY = '_'           #Leading character for dummy parent names.


#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
class Phage:
  """
  A phage.

  Attributes
  ----------
  name : str
      the name of the phage
  vlist : List
      the list of variants
  dummy : set, optional
      if non-empty, then this phage is a dummy parent of phages in the set
  """

  #Phage
  def __init__(self, name: str, vlist: Union[List, 'Phage'], dummy=set()):
    """
    Make an Phage.
    
    Parameters
    ----------
    name : str
        name of phage
    vlist : List
        the variants that describe this phage (with possibly WILDCARDs)
    dummy : set, optional
        if non-empty, then this phage is a dummy parent of phages in the set
    """
    self.name = name
    self.vlist = vlist
    self.dummy = dummy

  def getAlignStr(self) -> str:
    """
    Get a string with the name and the variant string.
    """
    return f'{self.name:>3}: '+str(self)

  def __str__(self) -> str:
    """
    Return a string with all the variants concatinated.
    
    note: This works as long as variants are single character. This is not
          true in general, since variants can be any string in our model.
    
    Returns
    -------
    str
        The variants concatinated with no spaces.
    """
    return ''.join(self.vlist)

  def __repr__(self) -> str:
    return str(self)

  def __len__(self) -> int:
    return len(self.vlist)

  @overload
  def __getitem__(self, i: int) -> str:
    """ Return a single element. """
  @overload
  def __getitem__(self, i: slice) -> List[str]:
    """ Return a slice. """
  def __getitem__(self, i) -> Union[str, List[str]]:
    """
    Return the ith element of the self.vlist.
    
    Parameters
    ----------
    i : int or Slice
        return this element or a slice
    """
    return self.vlist[i]

  def __setitem__(self, i: int, value: str):
    """
    Set the ith element of the self.vlist.
    
    Parameters
    ----------
    i : int
        set this element
    value : str
        set the element to this value
    """
    self.vlist[i] = value

  def __iter__(self):
    """
    Return a generator on the variant list.
    """
    return iter(self.vlist)

  def __eq__(self, other):
    """
    Compare phage variant, allowing for WILDCARDS.
    """
    for a, b in zip(self, other):
      if a != WILDCARD and b != WILDCARD and a != b:
        return False

    return True

  def __hash__(self):
    return hash(self.getAlignStr)

class NullPhage(Phage):
  def __init__(self):
    pass
NULL_PHAGE = NullPhage()


#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


communitynamere = re.compile(r'Community Name: (.+)\n')
alternativenamere = re.compile(r'(.+):\n')
def readTwoPhageCommunities(datafile: str) -> \
  Tuple[str, Dict[str, Phage], str, Dict[str, Phage]]:
  """
  Return a dictionary mapping the phage names to module variants.

  note:: Ignore blank lines or those with first character COMMENT_CHAR.

  note:: While a phage is represented by a list of variants (which could be
  anything that has an equality operator), the input format at the moment is
  not space-delimeted so a variant name is limited to a single character.

  Parameters
  ----------
  datafile: str
    the filename

  Returns
  -------
    cname1: str
      name of the first community
    c1: map str to list
      map phage name to Phage object (acts like a list)
    cname2: str
      name of the second community
    c2: map str to list
      map phage name to Phage object
  """
  cname1 = ''
  cname2 = ''
  c1: Dict[str, Phage] = {}
  c2: Dict[str, Phage] = {}
  numvars = 0
  with open(datafile) as f:
    for line in f:
      m = communitynamere.match(line)
      if m:                   #We have a community name.
        if not cname1:
          cname1 = m.group(1)
        elif not cname2:
          cname2 = m.group(1)
        else:
          raise(Exception('third community name specified on line: \n"{}"'.
                          format(line)))
      else:
        if line[0] != COMMENT_CHAR and cname1:
          if cname2:
            numvars = addToCommunity(line, c2, numvars)
          else:
            numvars = addToCommunity(line, c1, numvars)

  return cname1, c1, cname2, c2



def readPhageCommunities(datafile: str) \
  -> Tuple[Dict[str, Dict[str, Phage]], Dict[str, int]]:
  """
  Return a dictionary mapping community name to phage name to module variants.

  note:: Ignore blank lines or those with first character COMMENT_CHAR.

  note:: While a phage is represented by a list of variants (which could be
  anything that has an equality operator), the input format at the moment is
  not space-delimeted so a variant name is limited to a single character.

  Parameters
  ----------
  datafile: str
    the input filename

  Returns
  -------
  Dict[str, Dict[str, Phage]]
      map community name to phage name to Phage object
  Dict[str, int]
      map community name to order in file
  """
  with open(datafile) as f:
    return readPhageCommunitiesFILE(f)

def readPhageCommunitiesFILE(f: TextIO) \
  -> Tuple[Dict[str, Dict[str, Phage]], Dict[str, int]]:
  pass
  c = defdict(dict)   #CommunityName -> PhageName -> Phage
  cTOn = {}
  cname = ''
  numvars = 0
  counter = 0
  for line in f:
    if line.strip() == '' or line[0] == '#':
      continue

    comm = communitynamere.match(line)
    altm = alternativenamere.match(line)
    m = comm if comm else altm
    if m:                   #We have a community name.
      cname = m.group(1)
      counter += 1
      cTOn[cname] = counter  
    else:
      if not cname:
        raise(Exception(f'Expected first community name on line:\n"{line}"'))

      numvars = addToCommunity(line, c[cname], numvars)

  return c, cTOn


def addToCommunityList(line: str, community: List[Tuple[str, Phage]],
                       numvariants: int=0) -> int:
  """
  Parse the given phage line and add the phage to the community.

  Parameters
  ----------
    line: str
      the file line describing a phage

    community: List[Tuple[str, Phage]]
      list of phage name to Phage pairs

    numvariants: int
      the expected number of variants (0 if none expected)

  Returns
  -------
    numvariants: int
      the length of the phage on the given line
  """
  data = line.strip().split(':')
  name = data[0].strip()

  if name:
    if name in community:
      print('WARNING: two phages with the name "'+str(name)+'".')

    #if name[0] == DUMMY:
    #  raise(Exception(f'Phage name should not start with "dummy" character '
    #                  f'"{DUMMY}"."'))
    community[name] = Phage(name, list(data[1].strip()))

    if(numvariants and len(community[name]) != numvariants):
      sys.exit(f'ERROR: expected {numvariants} modules but got '+\
               f'{len(community[name])} for phage {name}.')

    numvariants = len(community[name])

  return numvariants


def addToCommunity(line: str, community: Dict[str, Phage],
                   numvariants: int=0) -> int:
  """
  Parse the given phage line and add the phage to the community.

  Parameters
  ----------
    line: str
      the file line describing a phage

    community: Dict[str, Phage]
      maps a phage name to a list of variants

    numvariants: int
      the expected number of variants (0 if none expected)

  Returns
  -------
    numvariants: int
      the length of the phage on the given line
  """
  data = line.strip().split(':')
  name = data[0].strip()

  if name:
    if name in community:
      print('WARNING: two phages with the name "'+str(name)+'".')

    #if name[0] == DUMMY:
    #  raise(Exception(f'Phage name should not start with "dummy" character '
    #                  f'"{DUMMY}"."'))
    community[name] = Phage(name, list(data[1].strip()))

    if(numvariants and len(community[name]) != numvariants):
      sys.exit(f'ERROR: expected {numvariants} modules but got '+\
               f'{len(community[name])} for phage {name}.')

    numvariants = len(community[name])

  return numvariants


def doRecombination(p1: Phage, p2: Phage,
                    b1: int, b2: int) -> Tuple[Phage, Phage]:
  """
  Recombine the phages at the given breakpoints (python indexed).
  
  Parameters
  ----------
  p1 : Phage
      first phage
  p2 : Phage
      second phage
  b1 : int
      breakpoint
  b2 : int
      breakpoint
  
  Returns
  -------
  Tuple[Phage, Phage]
      pair of phage products (the first of which has p1 surrounding p2)
  """
  assert(b1 < b2)
  return (Phage('', p1[:b1] + p2[b1:b2] + p1[b2:]),
          Phage('', p2[:b1] + p1[b1:b2] + p2[b2:]))


def printRecombination(p1: Phage, p2: Phage, c1: Optional[Phage],
                       c2: Optional[Phage], i:int, j:int) -> None:
  """
  Print the recombination between p1 and p2, producing c1 and c2.
  """
  print(f'Recombination:')
  print(f'{p1.name:>3}: {p1}')

  bpstr = [' ']*len(p1)
  bpstr[i-1] = '|'
  bpstr[i] = '|'
  bpstr[j-1] = '|'
  bpstr[j%len(p1)] = '|'
  print('     ' + ''.join(bpstr))

  print(f'{p2.name:>3}: {p2}')
  if c1:
    print(f'{c1.name:>3}: {c1}')
  if c2:
    print(f'{c2.name:>3}: {c2}')

