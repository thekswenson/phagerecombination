#Overview of the Program#

##The general process##

This process is repeated until there are no children left:

  1. If there is a recombination that creates a child, then **apply it** without scoring it, and start over.
  
  2. `Interval`s are computed between all pairs of parents.
  3. A minimum cover is computed with these intervals for each child.
  4. `FusedInterval`s are computed between all pairs of adjacent `Interval`s covering a child.
  5. Breakpoint pairs are scored by trying them. The score is the number of fused intervals that are remaining after application.
  6. Out of minimum scoring pairs, the first one is chosen arbitrarily.
  
##The breakpoint pairs that are considered##

Breakpoint pairs that operate on two `FusedInterval`s are considered with priority.  If none exist, then we consider each breakinterval for a `FusedInterval` along with all other possible breakpoints.