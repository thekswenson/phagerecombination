# Inference of Bacteriophage Recombination Histories #

These are tools for inferring recombination histories between communities of bacteriophages.

## Installation

(*These instructions assume a system running Ubuntu 19.04*)

These are steps to download the code and install the necessary dependencies.

1. Install dependencies using apt:
    `sudo apt install git python3-networkx`
2. Clone the repositories:
    `git clone --depth 1 git@bitbucket.org:thekswenson/phagerecombination.git`


## Usage

The main functionality of `recombinationdistance` is to compute recombination scenarios between phage communities. When run with no options, this file will output a recombination scenario as it is computed. Significant speedups will be observed when the `-p` parallelism is used.  Lower-bounds can be computed with the `--lowerbound` flag.

A sample run that computes a scenario from Factory 1 to Factory 2 is `recombinationdistance data/lacto/factories_1_2.txt`.  Use the `-p` flag to do computations on multple processors, *e.g.* `recombinationdistance -p 30 data/lacto/factories_1_2.txt`.

```
usage: recombinationdistance [-h] [-p NUM_PROCS] [--lowerbound] [--uncovered]
                             [--show-contained] [--cover] [-b]
                             [-i GRAPH_FILE.pdf] [-m] [--no-contained] [--all]
                             [--loners-not-last] [-v] [--very-verbose]
                             MODULES_TXT

Compute the recombination distance between two populations.

positional arguments:
  MODULES_TXT           the text file describing the modules (output of alpha
                        --modules)

optional arguments:
  -h, --help            show this help message and exit
  -p NUM_PROCS, --processors NUM_PROCS
                        the number of processors to use
  --lowerbound          only print the lowerbound
  --uncovered           only print the FusedIntervals uncovered by another
  --show-contained      print the FusedIntervals that are contained in another
  --cover               only print cover information
  -b, --breakpoints     only print information about the breakpoint intervals
  -i GRAPH_FILE.pdf, --interval-graph GRAPH_FILE.pdf
                        only build the interval graph
  -m, --leave-missing   do not deal with missing variants (WARNING)
  --no-contained        ignore FusedIntervals contained in another
  --all                 test all possible recombinations at each step
                        (slow/debug)
  --loners-not-last     a variant that occurs only in one child will
                        beinserted last (i.e. wildcarded) unless this flag is
                        given
  -v, --verbose         print extra information
  --very-verbose        print a lot of extra information
```



## Input file format

The factory files of the [`data/lacto/`](https://bitbucket.org/thekswenson/phagerecombination/src/master/data/lacto/) directory represent valid input files for `recombinationdistance`. In general, the format is as follows:

```
# A comment line starts with a pound character.
Community Name: NAME_1
PARENT_NAME_1: MODULES

Community Name: NAME_2
PARENT_NAME_2: MODULES
```

where `NAME_x` can be any string, `PARENT_NAME_x` is any non-space sequence of characters, and `MODULES` is a string of characters, where each character represents a variant. So the file:

```
Community Name: Factory 1
A: A---AAAA-A-A-AAAA-AA----A-AA-AAAAAAAAAA----AAAAAAA--AAA-AAAA-A-AA---A--A-A--A--AAAA-A
B: B---BAA-BABA-AABA-AA-B-B-BBB-BABBABAB-BB------BBABBBBBA-A--B-B-BB-----BB-BBBB-BBAAA-A
C: B---BA-A-A-A-AACA-ACC--C--CA-BCCCACCCCBC-CCCC-CCACCCBBACA--CCCCCC-C---CACC--CC-C-CACA
D: D---BA-D-A-D-AABA-DACD-D-BCA-DADDDDADDD-------DDABD-DBA-A-DD-ADDD-----BB-D--DC-DADADA
E: B-E-BAA--A-AEAACE-AAC--E--C--EAEEECAE-B------EEEAECCBBA-E--E-A-EE-C--E-A-E--E--EAEADE
F: B---BA-A-A-A-AACA-ACCF-C--CA-BCCCACCCCBC-CCCC-CCAFCCBBACA--CCCCFC-C---CACC--CC-F-CACA
G: D---BA-D-A-D-AABA-DAC--D-BCA-DADDEDAGDD-------DDABD-DBA-A-DD-ADDD-----BB-G--D--GADADA
H: BH--BA-A-A-A-AACA-ACC--C--CA-BCCCACCCCBC-CCCC-CCAHCCBBACA--CCCCHH-CH--BB-H--CC-HAEACA
I: B-E-BAA--A-AEAACE-AAC--E--C--EAEIECAE-B------EEIAECCBBA-E--I-A-EE-C----A-I--E--I-EA-E
J: A---AAAA-A-A-AAAA-AAJ---A-AA-AAAAAAAAAA----AAAJAAA--AAA-AAAJ-A-JJ---A--A-A--J--JAAA-A
K: D---BA-D-A-D-AABA-DAC--D-BCA-DADDDDADDD-------DDABD-DBA-A--K-ADKK---KKBB-K--DC-KAAADA
L: L---BAL----A-A-BALA-C-L---LA-DALLDLAL-B------ELLALL-DBA-L-DL-A-L--L----A-L--L--LAAADA
M: A---AAAA-A-A-AAAA-AAJ--MA-CA-AAAAAAAAAA----AAAMAAM-MMBM-AAAJ-A-MA---A--A-MM-J--MAAA-A

Community Name: Factory 2
N: N---AAAA-A-A-AAAA-AAJF--A-AA-AAAAAAAAAA----AAAMAAN-MMBM-AAAJ-A-MJ---A--A-N--A--NAAA-A
O: O---AAAA-A-A-AAAA-AAJF--A-AA-AAAAAAAAAA----AAAMAAO-MMBM-AAAJ-A-OO---A--A-O--A--OAAA-A
P: P---BAAA-A-A-AAAE-AAC--E--C--EAEPECAE-B------EEEAECCBBA-E--E-A--P-CH---A-P--E-BP-EA-E
Q: BH--BA-A-A-A-AAC--ACC--C--CA-BCCQACCCCBC-CCCC-CCAQCCBBACA--CCCCQH-CH--BB-Q--CC-QAEACA
```

represents a factory with 13 phages and a factory with 4 phages, each phage having 86 modules. The '-' character is used here to represent an empty module.
