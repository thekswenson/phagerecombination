# Copyright 2019 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
TODO: make a base class for intervals (overlaps() is duplicated between the
Interval and FusedInterval, for example).
"""

import copy

from typing import List
from typing import Tuple
from typing import Generator
from typing import Optional
from typing import Union
from typing import Set

from . import PAIR
from .Phage import Phage
from .Phage import WILDCARD
from .Phage import EMPTY_CHAR
from .Phage import DUMMY
from .Phage import NULL_PHAGE


#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
class Interval:
  """
  This is an interval that covers a phage.
  Keep track of the parent phage, not the child.

  note:: Intervals that wrap will have an end index greater than the phage
         length.

  Attributes
  ----------
  start: int
    the start of the interval
  end: int
    the end of the interval (python indexed)
  phage: List
    the phage
  wraplen: int
    the length of the phage
  phagename: str
  s: List
    the slice corresponding to the interval

  start_essential: int
    the start of the "essential part" of the interval (that is not covered by
    anything else in the minimum cover). this will always be an in index from
    0 to self.wraplen-1.
  end_essential: int
    the end of the essential part of the interval. this could have a value
    greater than self.wraplen if the interval wraps.
  """

  #Interval
  def __init__(self, start, end, phage: Phage=NULL_PHAGE, phagename='',
               wraplen=0):
    """
    Parameters
    ----------
    phage: Phage
      the phage
    phagename: str
      the covering phage
    wraplen: str
      the wrap length for the intervals (ignored if phage is specified)
    """
    self.start = start
    self.end = end
    self.phagename = phagename
    self.phage: Phage = phage

    if phage is not NULL_PHAGE:
      self.setS()
    else:
      self.s = []
      if wraplen:
        self.wraplen = wraplen

    self.start_essential: int = -1
    self.end_essential: int = -1


  def setS(self) -> None:
    """
    Innitialize the slice s.
    """
    if self.end > len(self.phage):
      self.s = self.phage[self.start:] + self.phage[:self.end-len(self.phage)]
    else:
      self.s = self.phage[self.start:self.end]

    self.wraplen = len(self.phage)
    assert(len(self.s) == len(self))

  def switchPhage(self, phage: Phage) -> None:
    """
    Change the phage that this interval is applied to.
    
    note:: the slice s is changed accordingly
    
    Parameters
    ----------
    phage : Phage
        the new phage to use
    """
    self.phage = phage
    self.phagename = phage.name
    self.setS()

  def touchesBefore(self, other: 'Interval'):
    """
    Return True if this Interval touches other (they overlap or one directly
    follows the other) and is before other (in the circular ordering).
  
    >>> inter = Interval(0, 2, wraplen=4)   #Overlaps.
    >>> inter.touchesBefore(Interval(1,3, wraplen=4))
    True
    >>> inter.start, inter.end = (0,1)      #Touches without overlapping.
    >>> inter.touchesBefore(Interval(1,3, wraplen=4))
    True
    >>> inter.start, inter.end = (0,0)
    >>> inter.touchesBefore(Interval(1,3, wraplen=4))
    False
    >>> inter.start, inter.end = (3,5)      #Second interval wraps around.
    >>> inter.touchesBefore(Interval(1,2, wraplen=4))
    True
    >>> inter.start, inter.end = (3,5)      #Second interval wraps around.
    >>> inter.touchesBefore(Interval(2,3, wraplen=4))
    False
    >>> inter.start, inter.end = (1,4)      #Containment.
    >>> inter.touchesBefore(Interval(2,3, wraplen=4))
    True
    """
    if self.start > other.start:
      return self.end >= self.wraplen and self.end-self.wraplen >= other.start
  
    return self.end >= other.start

  def getOverlapRight(self, other: 'Interval') -> PAIR:
    """
    Return a pair that represents the overlap of the right side of self
    with the left side of the given interval.
    
    Parameters
    ----------
    other : Interval
        the interval to the right of this one
    
    Returns
    -------
    PAIR
        the indices where there is an overlap
    """
    if((self.start > other.start and self.end%self.wraplen < other.start) or
       (self.start < other.start and self.end < other.start)):
      return (-1, -1)

    if other.start == 0:
      start = self.wraplen
    else:
      start = other.start 
    if self.wraps():
      end = self.end - self.wraplen
    else:
      end = self.end

    return (start, end)


  def overlaps(self, other: 'Interval') -> Optional[PAIR]:
    """
    Return the overlapping interval between this Interval and `other`, otherwise
    return None.
    """
    return overlapPairs(self.asPair(), other.asPair(), self.wraplen)


  def leftOverlaps(self, other: 'Interval') -> Optional[PAIR]:
    """
    Return the overlapping interval between the two, with the contraint that
    `self` is to the left of `other`.
    """
    return leftOverlaps(self.asPair(), other.asPair(), self.wraplen)


  def notempty(self):
    return self.phagename

  def empty(self):
    return not self.phagename

  def getAlignStr(self):
    """
    Return a string with the characters of the interval in the proper location.
    """
    return getAlignStr(self)

  def wraps(self, totheend=False) -> bool:
    """
    Does this interval wrap around to the beginning?
    
    Parameters
    ----------
    totheend : bool, optional
        return True when the sequence goes to the end but doesn't wrap, by
        default False
    """
    return self.end > self.wraplen or (totheend and self.end == self.wraplen)

  def phageIndexEnd(self: 'Interval'):
    """
    Return the end index wrapped to the beginning if necessary.
    """
    return self.end % self.wraplen


  def coversIndex(self, i: int) -> bool:
    """
    Return True if the given (phage) index is covered by this Interval.

    note:: an index equal to this Interval's (Python indexed) end is not in
           this Interval
    
    Parameters
    ----------
    i : int
        the index
    """
    assert(0 <= i and i < self.wraplen)
    return ((i >= self.start and i < min(self.end, self.wraplen)) or
            (self.end > self.wraplen and i < self.end - self.wraplen))


  def phageIndexIter(self, essentialonly=False) -> Generator:
    """
    Iterate over the phage indices (wrap the indices).

    Parameters
    ----------
    essentialonly : bool
        only check the essential part of this interval
    """
    if essentialonly:
      start = self.start_essential
      end = self.end_essential
    else:
      start = self.start
      end = self.end

    for i in range(start, min(end, self.wraplen)):
      yield i

    if end > self.wraplen:
      for i in range(end - self.wraplen):
        yield i

  def occursIn(self, phage: Phage, selfwildcards: bool=False,
               essentialonly: bool=False) -> bool:
    """
    Does this interval occur in the given phage?

    Parameters
    ----------
    phage : Phage
        the phage to test
    selfwildcards : bool
        check for wildcards in self rather than phage?
    essentialonly : bool
        only check the essential part of this interval
    """
    assert(self.phage)

    for i in self.phageIndexIter(essentialonly):
      if(self.phage[i] != phage[i] and
         ((selfwildcards and self.phage[i] != WILDCARD) or
          (not selfwildcards and phage[i] != WILDCARD))):
        return False

    return True

  def isDummy(self) -> bool:
    """
    Return True if this interval has a dummy parent.
    
    We use the name (starting with the DUMMY character) to determine this.
    """
    return isDummyName(self.phagename)

  def __getitem__(self, i):
    """
    Return the element that is at this PHAGE index.

    note:: return the empty element if given index is outside the interval
    """
    assert(i < self.wraplen)
    if(i >= self.end or (i < self.start and not self.wraps()) or
       (i < self.start and self.wraps() and i >= self.end - self.wraplen)):
      return EMPTY_CHAR

    if self.wraps() and i < self.start:
      return self.s[i + (self.wraplen - self.start)]

    return self.s[i - self.start]

  def getExtended(self, phage: Phage, limitint: 'Interval'=None) -> 'Interval':
    """
    Extend this interval to either side as long as self.phage matches phage.
    
    Parameters
    ----------
    phage : Phage
        This phage must match self.phage in extended regions.
    limitint : Interval, default None
        If specified do not extend past the limits of this interval
    
    Returns
    -------
    Interval
        The new extended interval
    """
    newinterval = copy.deepcopy(self)
    newinterval.extend(phage, limitint)
    return newinterval

  def extend(self, phage: Phage, limitint: 'Interval'=None) -> None:
    """
    Extend this interval to either side as long as self.phage matches phage.
    
    Parameters
    ----------
    phage : Phage
        This phage must match self.phage in extended regions.
    limitint : Interval, default None
        If specified do not extend past the limits of this interval
    """
    start = self.start-1
    while((self.phage[start] == WILDCARD or phage[start] == WILDCARD or
           self.phage[start] == phage[start]) and
          (not limitint or start >= limitint.start)):
      start -= 1
    start += 1

    end = self.end
    while((self.phage[end%self.wraplen] == WILDCARD or
           phage[end%self.wraplen] == WILDCARD or
           self.phage[end%self.wraplen] == phage[end%self.wraplen]) and
          (not limitint or end <= limitint.end)):
      end += 1
    
    if end > self.wraplen:          #The new end wraps.
      if self.end > self.wraplen:   #The old end wraps.
        endslice = self.phage[self.end-self.wraplen: end-self.wraplen]
      else:
        endslice = self.phage[self.end:] + self.phage[:end-self.wraplen]
    else:
      endslice = self.phage[self.end: end]

    if start < 0:
      beginslice = self.phage[start:] + self.phage[:self.start]
      start = self.wraplen + start
      end = self.wraplen + end
    else:
      beginslice = self.phage[start: self.start]

    self.s = beginslice + self.s + endslice
    self.start = start
    self.end = end


  def setEssentialInterval(self, i: int, j: int) -> None:
    """
    Set the essential interval for this interval.
    This is the part of the interval that cannot be covered by another phage
    in the minimum cover we computed.
    
    Parameters
    ----------
    i : int
        start index
    j : int
        end index
    """
    assert(i <= j)
    assert(self.coversIndex(i))
    assert(self.coversIndex((j-1)%self.wraplen))

    self.start_essential = i
    self.end_essential = j

  def getEssentialPair(self) -> PAIR:
    """
    Get the essential interval for this interval.
    This is the part of the interval that cannot be covered by another phage
    in the minimum cover we computed.
    
    Return
    ----------
    PAIR
        The pair (self.start_essential, self.end_essential)
    """
    return self.start_essential, self.end_essential


  def asPair(self) -> PAIR:
    """
    Return the pair (self.start, self.end).
    
    Returns
    -------
    PAIR
        the start and end as a pair
    """
    return (self.start, self.end)

  def getEssentialHighlightStr(self, highlight='_') -> str:
    """
    Return a string highlighting the essential interval.
    
    Parameters
    ----------
    highlight : str, optional
        the character to use to highlight, by default '_'
    """
    line = ''
    #print(f'getEssentialHighlightStr() {self.start_essential} {self.end_essential}')
    if self.end_essential <= self.wraplen:
      length = self.end_essential - self.start_essential
      line += ' '*(self.start_essential) + highlight*length
    else:
      length = self.end_essential - self.wraplen
      line += highlight*length
      line += ' '*(self.start_essential - length)
      line += highlight*(self.wraplen - self.start_essential)

    return line

  #def __sub__(self, other: 'Interval') -> 'Interval':
  #  """
  #  Remove the other interval from this one.

  #  warn:: unused and untested!
  #  
  #  Parameters
  #  ----------
  #  other : Interval
  #      the Interval to remove from this one.
  #  
  #  Returns
  #  -------
  #  Interval
  #      a new Interval that is the difference of this and the other
  #  """
  #  if self.touchesBefore(other):
  #    if other.start < self.start:        #self wraps
  #      end = self.wraplen + other.start
  #    else:
  #      end = other.start

  #    return Interval(self.start, end, self.phage, self.phagename, self.wraplen)

  #  elif other.touchesBefore(self):
  #    if other.end > other.wraplen:       #other wraps
  #      start = other.end - other.wraplen
  #    else:
  #      start = other.end

  #    return Interval(start, self.end, self.phage, self.phagename, self.wraplen)

  #  return copy.deepcopy(self)


  def __str__(self):
    return f'({self.start}, {self.end})'

  def __repr__(self):
    return str(self)

  def __len__(self):
    return self.end - self.start

  def __lt__(self, other: 'Interval'):
    if self.start == other.start:
      return self.end < other.end

    return self.start < other.start

  def __eq__(self, other):
    if not isinstance(other, Interval):
      raise NotImplementedError

    return self.start == other.start and self.end == other.end

  def __le__(self, other):
    return self < other or self == other

  def __hash__(self):
    return hash((self.start, self.end))

  def __contains__(self, other: 'Interval') -> bool:
    if self.wraps() and not other.wraps():
      return self.start <= other.start or self.end-self.wraplen >= other.end

    if not self.wraps() and other.wraps():
      return False
                          #They both wrap or neither wraps now.
    return self.start <= other.start and self.end >= other.end


class NullInterval(Interval):
  def __init__(self):
    pass
NULL_I = NullInterval()


#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
class ContentInterval(Interval):
  """
  An Interval with equality operation and hash function that considers the
  content of the interval, and not just the interval indices themselves.
  
  note:: this is for use in sets when the content matters
  """
  def __init__(self, interval: Interval):
    self.start = interval.start
    self.end = interval.end
    self.parent = interval.parent
    self.parentname = interval.parentname
    self.s = interval.s
    self.wraplen = interval.wraplen

  def matchesSubIntervalOf(self, other):
    """
    Return True if this ContentInterval matches a subinterval of the other.
    
    This is true if the variants are the same for the two intervals in the
    same locations.
    
    Parameters
    ----------
    other : [type]
        other ContentInterval that contains this one
    """
    if self in other:
      diff = other.start - self.start
      if self.s == other.s[diff: diff + len(self)]:
        return True

    return False

  def __eq__(self, other):
    if not isinstance(other, ContentInterval):
      raise NotImplementedError

    return (self.start == other.start and self.end == other.end and
            self.s == other.s)

  def __hash__(self):
    return hash((self.start, *self.s))



#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
class FusedInterval:
  """
  Represent two compatible (possibly overlapping) intervals that are "fused" at
  their edges. These are intervals that can be put together with a
  recombination operating at the overlap.  An example would be::

      ab.
      .bc

  Attributes
  ----------
  start: int
    start index of the interval

  end: int
    end index of the interval

  start_bi: int
    start of the breakinterval. the last unique character of the first interval
    is at index start_bi-1. if the breakinterval starts at the beginning of the
    sequence, then this start_bi = wraplen. see the docstring of
    `setBreakInterval` for more information.

  end_bi: int
    end of the breakinterval. the first unique character of the second interval
    is at index end_bi.

  first: Interval
    an Interval

  second: Interval
    another Interval that overlaps or abuts to the right of first

  child: Phage
    the phage that is covered by this Interval (or an extension of it)

  childname: str
    the phage that is covered by this Interval (or an extension of it)

  s: List
    the slice corresponding to the fused interval

  c: List
    the complement of the slice corresponding to the fused interval (i.e.
    take the element from the opposite phage than s)

  wraplen: int
    the length of the phage

  essential: Interval
    the essential part of the breakpoint, which is the breakinterval plus
    one module to either side.
  """

  #FusedInterval
  def __init__(self, i1: Interval, i2: Interval, childname: str, child: Phage):
    """
    Parameters
    ----------
    i1: Interval
      the first interval to make part of the fused interval
    i2: Interval
      the second interval to make part of the fused interval
    childname: str
      that name of the child that this fused interval covers
    child: Phage
      the child that this fused interval covers
    #i1left: Interval
    #  the interval before the first interval
    #i2right: Interval
    #  the interval to the right of the second interval
    """
    if i1.empty() or i2.empty():
      raise(Exception('trying to fuse an empty interval.'))

      #Decide which is first in the circular ordering:
    self.coversall: bool = False
    if coversAllIndicesPair(i1, i2):  #If the intervals cover a whole phage
      self.coversall = True
      first, second = i1, i2          # then preserve the order.
    else:
      first, second = orderIntervals(i1, i2)

    self.first: Interval = first
    self.second: Interval = second
    self.childname: str = childname
    self.child: Phage = child
    self.wraplen: int = first.wraplen
    self.extendedbi: Interval = NULL_I

    if self.coversall:
      self.start, self.end = self.getStartEndCoversall()
    else:
      self.start = first.start
      if second.end <= first.start:
        self.end = second.end + self.wraplen      #the FusedInterval wraps
      else:
        self.end = second.end

    self.setBreakInterval()

      #Build the complement intervals (take the interval that we use on first
      # and define it on second, and vice-versa). Use this to define self.c.
    firstc = Interval(first.start, first.end, second.phage, second.phagename)
    secondc = Interval(second.start, second.end, first.phage, first.phagename)

    if first.wraps(True) and not second.wraps():
      self.s = first.s + second.s[(first.end-first.wraplen) - second.start:]
      self.c = firstc.s + secondc.s[(first.end-first.wraplen) - second.start:]
    else:
      self.s = first.s + second.s[first.end - second.start:]
      self.c = firstc.s + secondc.s[firstc.end - secondc.start:]

  def getStartEndCoversall(self) -> PAIR:
    """
    Return the start and end indices, where the end index is always greater than
    the start, for this FusedInterval assuming the first and second intervals
    cover all of the indices.

    Returns
    -------
    PAIR
        The start and end indices of the this FI.
    """
    if self.first.start < self.second.start:
      # First starts before second:
      # Case 1:  ----|    |-------------------
      #          ----------------|    |-------
      #
      # Case 2:    |-------------|
      #          ----|    |-------------------
      #
      return self.first.start, self.second.end

      # Second starts before first:
      # Case 3:  ----------------|    |-------
      #          ----|    |-------------------
      #
      # Case 4:  ----|    |-------------------
      #            |-------------|
      #
    return self.first.start, self.second.end + self.wraplen
    

  def setBreakInterval(self) -> None:
    """
    Compute the indices of the breakinterval.

    Indices are different from normal indexing, for example
    __
      _____

    will have breakinterval (2,2).
    If the breakinterval wraps then the end_bi will be greater than wraplen,
    otherwise start_bi and end_bi are always less than wraplen.
    """
    if self.coversall:
      if self.first.start < self.second.start:
        # First starts before second:
        # Case 1:  ****|    |------------*******
        #          ****------------|    |*******
        #
        # Case 2:    |-------******|
        #          ----|    |******-------------
        #
        self.start_bi = self.second.start
        self.end_bi = self.first.end
      else:
        # Second starts before first:
        # Case 3:  ----------******|    |-------
        #          ----|    |******-------------
        #
        # Case 4:  ---*|    |-------------------
        #            |*------------|
        #
        self.start_bi = self.second.start + self.wraplen
        self.end_bi = self.first.end
      
    else:
      self.start_bi = self.second.start 

      if((self.first.wraps() or self.second.start == 0) and
         not self.second.wraps()):
        self.end_bi = self.first.end - self.first.wraplen
      else:
        self.end_bi = self.first.end


  def setExtendedBI(self):
    """
    Set the extended breakpoint interval of this FI, which contains the
    breakinterval plus one module to either side.  The `extendedbi` is
    used to calculate a lower bound.
    """
    start = (self.start_bi - 1) % self.wraplen
    end = start + self.end_bi - self.start_bi + 2
    self.extendedbi = Interval(start, end, wraplen=self.wraplen)
    self.extendedbi.s = self.slice(start, end)
    self.extendedbi.phage = Phage('', getAlignList(self.extendedbi))
  
  
  def coversAllIndices(self):
    """
    Does this fused interval cover all of the phage indices.
    
    note:: if so, then there is a recombination that creates a child
    """
    return self.end - self.start >= self.wraplen

  #def compatibleWith(self, other: 'FusedInterval'):
  #  """
  #  Can these FusedIntervals be created with a single recombination?

  #  They are fuseable if they share the same parents and

  #    1) the parents are in the same order and 
  #      a. one is contained in (or is a trivial extension of) the other, or
  #      b. they are different for all overlapping positions (when the parents
  #         differ).
  #    
  #    2) the parents appear in the opposite order and 
  #      a. are the same for all overlapping positions.

  #  
  #  Parameters
  #  ----------
  #  other : FusedInterval
  #      the other FusedInterval
  #  """
  #  if(self.first.parentname == other.first.parentname and #Parents in 
  #     self.second.parentname == other.second.parentname): #same order
  #    print('compatibleWith? same order')
  #    print(f'self:  {self.getAlignStr()} {self.first.parentname} {self.second.parentname} {self}?')
  #    print(f'other: {other.getAlignStr()} {other.first.parentname} {other.second.parentname} {other}?')

  #    ol = self.overlaps(other)
  #    if ol and self.slice(*ol) == other.slice(*ol):
  #      print(f'\tYES')
  #      return True

  #  raise(NotImplementedError)

  def hasHealTwoWith(self, other: 'FusedInterval') -> bool:
    """
    Return True if other can be used to the right of self in a recombination
    that heals two breakpoints.

    For example, the following fused intervals can be healed in a single move
      FI1 A: ----------
          B:       ---------

      FI2 B:                      -------
          A:                           ---------

    since they look like this:

          A: ----------                ---------
          B:       ---------      -------

    Parameters
    ----------
    other : FusedInterval
        the FI to the right

    Returns
    -------
    bool
        True if the other FI can combine with this to be used in a recombination
        that heals two breakpoints.
    """
      #Must be able to go from phage A to B and back:
    if(self.first.phagename != other.second.phagename or
       other.first.phagename != self.second.phagename):
      return False

      #One break-interval is contained in the other:
    if self.start_bi == other.start_bi:
      return False
    
      #Ensure the start of fi1's break-interval is left of the start BI for fi2:
    fi1 = self
    fi2 = other
    if self.start_bi > other.start_bi:
      fi1 = other
      fi2 = self

      #Ensure that essential parts are included:
      #  Important indices must be ordered properly. Add the wraplen to the
      #  indices if need be so that they can be compared directly.
      #
      #                   i6    i1  i8    i2 i3   i5 i4   i7
      # fi1.first  = A: --**---|---|
      #    .second = B:        |---|-------***
      #
      # fi2.first  = B:                  -**-----|--|
      #    .second = A:                          |--|--****--
      #
    _, fi1_second_end_essential = fi1.getSecondEssentialPair()
    _, fi2_second_end_essential = fi2.getSecondEssentialPair()
    i1 = fi1.start_bi
    i2 = fi2.first.start_essential
    i3 = fi1_second_end_essential

    wraplen = self.wraplen
    i4 = fi2.end_bi
    if not fi2.first.wraps():
      i4 += wraplen

    i5 = fi2.start_bi     #BI starting at wraplen has start_bi = wraplen (not 0)

    i6 = fi1.first.start_essential
    if i6 < fi1.start_bi:
      i6 += wraplen

    i7 = fi2_second_end_essential
    if i7 < fi2.start_bi:
      i7 += wraplen

    i8 = fi1.end_bi + wraplen

    if i2 < i1 or i3 > i4 or i6 < i5 or i7 > i8:
      #print(f'REJECTED: ({wraplen})')
      #print(fi1.getAlignStrTwoLine(True))
      #print(fi2.getAlignStrTwoLine(True))
      #if i2 < i1:
      #  print(f'  i2 < i1 ({i2} < {i1})')
      #if i3 > i4:
      #  print(f'  i3 > i4 ({i3} > {i4})')
      #if i6 < i5:
      #  print(f'  i6 < i5 ({i6} < {i5})')
      #if i7 > i8:
      #  print(f'  i7 > i8 ({i7} < {i8})')
      return False

    return True


  def slice(self, start: int, end: int, phageindices=True,
            complement=False) -> List[str]:
    """
    Return a slice corresponding to the given interval's sequence.
    
    Parameters
    ----------
    start : int
        the first index of the interval
    end : int
        the second index of the interval
    phageindices : bool
        the given indices are phage indices. If False, then `end` and `start` must
        both be greater than `self.start`. If True, then `end` may be smaller
        than `start`, both are in the range [0, `self.wraplen`].
    complement : bool
        return the slice from the complement self.c
    """
    if phageindices and self.wraps():
      if start < self.start:
        start += self.wraplen
        assert start <= self.end, f'Phage start index {start} too big!'

      if end < self.start:
        end += self.wraplen
        assert end <= self.end, f'Phage end index {end} too big!'

      #All indices now are within the interval [self.start..self.end]
    assert self.start <= start <= self.end
    assert self.start <= end <= self.end

    if complement:
      return self.c[start - self.start : end - self.start]

    return self.s[start - self.start : end - self.start]


  def overlaps(self, other: 'FusedInterval') -> Optional[PAIR]:
    """
    Does this interval overlap the other one?

    note:: only looks at the indices, ignoring the parentnames and content.
    
    Parameters
    ----------
    other : FusedInterval
        the other interval
    
    Returns
    -------
    PAIR
        The interval over which the two intervals overlap, or None if they don't
    """
    if self.coversall:
      return other.asPair()
    elif other.coversall:
      return self.asPair()

    return overlapPairs(self.asPair(), other.asPair(), self.wraplen)

  def overlapsBI(self, other: 'FusedInterval') -> Optional[PAIR]:
    """
    Does this Break Interval overlap the other one's?

    note:: only looks at the indices, ignoring the parentnames and content.
    
    Parameters
    ----------
    other : FusedInterval
        the other interval
    
    Returns
    -------
    PAIR
        The interval over which the two intervals overlap, or None if they don't
    """
    return overlapPairs(self.getBIPair(True), other.getBIPair(True),
                        self.wraplen)


  def asPair(self) -> PAIR:
    """
    Return the pair (self.start, self.end).
    """
    return self.start, self.end

    
  def parentPair(self) -> Tuple[Phage, Phage]:
    return (self.first.phage, self.second.phage)


  def coversIndex(self, i: int) -> bool:
    """
    Return True if the given (phage) index is covered by this FusedInterval.

    note:: an index equal to this FI's (Python indexed) end is not in this FI
    
    Parameters
    ----------
    i : int
        the index
    """
    assert(0 <= i and i < self.wraplen)
    return ((i >= self.start and i < min(self.end, self.wraplen)) or
            (self.end > self.wraplen and i < self.end - self.wraplen))



  def contains(self, other: 'FusedInterval', ignoreparents: bool=False,
               complement: bool=False, printcontainment: bool=False) -> bool:
    """
    Does this interval contain the other one?

    Other FI must be a substring of this, and the parents must be the same,
    unless ignoreparents is True.

    note:: if other has a WILDCARD at position i, then self and other don't
           need to match at position i.
    
    Parameters
    ----------
    other : FusedInterval
        the other FI
    ignoreparents : bool
        ignore the parent names in the test
    complement : bool
        also consider the complement of self for containment
    printcontainment : bool
        print a message for those that are contained
    """
      #Check parents:
    if(not ignoreparents and
       (self.first.phagename != other.first.phagename or
        self.second.phagename != other.second.phagename) and
       (self.first.phagename != other.second.phagename or
        self.second.phagename != other.first.phagename)):
      return False

      #Check interval containment:
    o = self.overlaps(other)                   #get overlapping interval

      #TEMPORARY CHECK
    assert (bool(o) and o[1] - o[0] == len(other)) == self.indicesContain(other)

    if not o or o[1] - o[0] != len(other):     #other not contained in self
      return False

      #Check breakinterval overlap:
    if not overlapPairs(self.getBIPair(True), other.getBIPair(True),
                        self.wraplen):
      return False

      #Check matching sequence:
    if complement:
      for i in range(*o):
        i = i % self.wraplen

        if self.first.coversIndex(i):
          cself = self.second.phage[i]
        else:
          cself = self.first.phage[i]

        if cself != other.child[i] and other.child[i] != WILDCARD:
          return False

    else:
      for i in range(*o):
        i = i % self.wraplen
        tocover = other.child[i]     #Can the child covered by other not be
        #if self.child[i] != other.child[i] and other.child[i] != WILDCARD:
        if(tocover != WILDCARD and   #covered by the parents of self?
           ((self.first.coversIndex(i) and self.first[i] != tocover) or
            (self.second.coversIndex(i) and self.second[i] != tocover))):
          return False

    #if(self.childname == 'f' and other.childname == 'j' and
    #   self.getBIPair() == (35, 35) and other.getBIPair() == (35, 35)):
    #  print(f'MATCH')
    #  print(f'  {"".join(self.first.phage[o[0]: o[1]])}')
    #  print(f'  {"".join(self.second.phage[o[0]: o[1]])} covers')
    #  print(f'  {"".join(other.child[o[0]: o[1]])}')

    if printcontainment:
      print(f'CONTAINMENT: {self.childname} \n{self.getAlignStrTwoLine()}')
      print(f'   contains: {other.childname} \n{other.getAlignStrTwoLine()}')
      #print(f'   {self} {other}')

    return True

  def coversCommon(self, other: 'FusedInterval') -> bool:
    """
    Return True if the parents of the overlapping part of self covers the
    overlapping part of the child of other (BIs must overlap too).
    
    Parameters
    ----------
    other : FusedInterval
        the one to be covered
    """
    if not overlapPairs(self.getBIPair(True), other.getBIPair(True),
                        self.wraplen):
      return False             #BreakIntervals don't overlap

    o = self.overlaps(other)   #get the overlapping interval
    if not o:
      return False             #no overlap

    for i in range(*o):
      i = i % self.wraplen
      tocover = other.child[i]     #Can the child covered by other, not be
      if(tocover != WILDCARD and   #covered by the parents of self?
         ((self.first.coversIndex(i) and self.first[i] != tocover) or
          (self.second.coversIndex(i) and self.second[i] != tocover))):
        return False

    #print(f'overlap: {o}')
    #print(self.getAlignStrTwoLine())
    #print(other.getAlignStrTwoLine())

    return True

  def coversExtension(self, other: 'FusedInterval') -> bool:
    """
    Return True if the parents of the self covers the the child of other for
    the entire combined interval of the two (BIs must overlap too).

    For example::
    self       _______
                      ---------
    other 0_____________
                  -----------------n

    self must cover the child of other from 0 all the way to n.
    
    Parameters
    ----------
    other : FusedInterval
        the one to be covered
    """
    raise(NotImplementedError)


  def covers(self, other: 'FusedInterval') -> bool:
    """
    Return True if the parents of the overlapping part of self covers the
    overlapping part of the child of other (BIs must overlap too).
    
    Parameters
    ----------
    other : FusedInterval
        the one to be covered
    """
    if not overlapPairs(self.getBIPair(True), other.getBIPair(True),
                        self.wraplen):
      return False             #BreakIntervals don't overlap

    o = self.overlaps(other)   #get the overlapping interval
    if not o:
      return False             #no overlap

    for i in range(*o):
      i = i % self.wraplen
      tocover = other.child[i]     #Can the child covered by other, not be
      if(tocover != WILDCARD and   #covered by the parents of self?
         ((self.first.coversIndex(i) and self.first[i] != tocover) or
          (self.second.coversIndex(i) and self.second[i] != tocover))):
        return False

    #print(f'overlap: {o}')
    #print(self.getAlignStrTwoLine())
    #print(other.getAlignStrTwoLine())

    return True


  def indicesContain(self, other: 'FusedInterval') -> bool:
    """
    Do the indices of this interval contain the indices of the other?
    
    Parameters
    ----------
    other : FusedInterval
        the other FI
    """
    if self.coversall:
      return True

    if self.wraps():                                #self  ------|     |-------
      wrapedend = self.end - self.wraplen
      return ((other.start < wrapedend and          #other  |--|
               other.end <= wrapedend) or
              (other.start >= self.start and        #                    |--|
               other.end <= self.end))              #  or  ---|           |----

    else:                                           #self   |---------------|
      return (other.start >= self.start and         #other        |----|
              other.end <= self.end)


  def wraps(self: 'FusedInterval'):
    """
    Does this interval wrap?
    """
    return self.end > self.wraplen

  def wrapsBI(self: 'FusedInterval'):
    """
    Does this breakpoint interval wrap?
    """
    return self.end_bi > self.wraplen

  def phageIndexEnd(self: 'FusedInterval'):
    """
    Return the end index wrapped to the beginning if necessary.
    """
    return self.end % self.wraplen

  def inBreakInterval(self, i: int) -> bool:
    """
    Return True if the given index is in the breakinterval.
    
    Parameters
    ----------
    i : int
        the index
    
    Returns
    -------
    bool
        True if i is in the breakinterval
    """
    return i >= self.start_bi and i <= self.end_bi


  def getAlignStr(self, complement=False) -> str:
    """
    Return a string with the characters of the interval in the proper location.

    Parameters
    ----------
    complement: bool
      print the complement slice instead of the normal slice
    """
    return getAlignStr(self, complement)

  def getAlignStrTwoLine(self, showessential=False) -> str:
    """
    Return a string with the characters of the intervals in the proper location.
    Do this on two lines with the first and second intervals on their own lines.
    """
    firstline = ''
    lastline = ''
    leadingspace = ' '*5
    if showessential:
      firstline = leadingspace + self.first.getEssentialHighlightStr() + '\n'
      lastline = '\n' + leadingspace + self.second.getEssentialHighlightStr('-')

    return f'{firstline}{self.first.phagename:>3}: {self.first.getAlignStr()}'+\
           f'\n{self.second.phagename:>3}: {self.second.getAlignStr()}{lastline}'


  def phageIndexIter(self, start=None):
    """
    Iterate over the phage indices covered by this FI (wrap the indices).

    Parameters
    ----------
    start : int
        the index to start at, rather than self.start (must be in interval)
    """
    if start == None:
      start = self.start
    else:
      assert(self.coversIndex(start))

    if start >= self.start:
      for i in range(self.start, min(self.end, self.wraplen)):
        yield i

      start = 0

    if self.wraps():
      for i in range(start, min(self.end - self.wraplen, self.start)):
        yield i


  def phageIndexBIIter(self, oneleft=False, oneright=False):
    """
    Iterate over the phage indices of the breakinterval (wrap the indices).
    """
    if oneleft:
      if self.start_bi > 0:
        yield self.start_bi - 1
      else:
        yield self.wraplen - 1

    for i in range(self.start_bi, min(self.end_bi, self.wraplen)):
      yield i

    if self.end_bi > self.wraplen:
      for i in range(self.end_bi - self.wraplen):
        yield i

    if oneright:
      if self.end_bi + 1 > self.wraplen:
        yield self.end_bi - self.wraplen
      else:
        yield self.end_bi

  def isBreakIntervalIndex(self, i: int, oneleft=False, oneright=False):
    """
    Is this (phage) index part of the breakinterval?
    
    Parameters
    ----------
    i : int
        the index
    oneleft : bool
        include one index to the left
    oneright : bool
        include one index to the right
    """
    assert(i < self.wraplen and i >= 0)

    if self.end_bi > self.wraplen and i < self.end_bi - self.wraplen:
      return True              #The interval wraps and i is in the beginning

    if i > self.start_bi and i < min(self.start_bi, self.wraplen):
      return True

    if oneleft and i == self.start_bi - 1:
      return True

    if oneright:
      if(i == self.end_bi or   #Could use % here but don't for clarity:
         (self.end_bi > self.wraplen and i == self.end_bi - self.wraplen)):
        return True

    return False


  def getBIPair(self, pythonindexed=False) -> PAIR:
    """
    Return the breakinterval b. b[0]-1 is the index of last non-shared character
    of the first interval. b[1] is the index of the first non-shared character
    of of the second interval.
    
    
    Parameters
    ----------
    pythonindexed : bool, optional
        should the ending index be non-inclusive?, by default False
    """
    end = self.end_bi
    if pythonindexed:
      end += 1

    return self.start_bi, end


  def hasDummyParent(self) -> bool:
    """
    Return true if one of the parents is a dummy.
    """
    return self.first.isDummy() or self.second.isDummy()

  def getDummyBreakpoints(self) -> PAIR:
    """
    Return the Interval start and end for the dummy parent.
    
    note:: if the Interval wraps, then return the equivalent breakpoints that
           don't wrap
    
    Returns
    -------
    PAIR
        the two breakpoints
    """
    dummy = self.first if self.first.isDummy() else self.second

    if not dummy.isDummy():
      raise(Exception('one of the two parents must be a dummy!'))

    if dummy.wraps():
      return (dummy.end - dummy.wraplen, dummy.start)

    return (dummy.start, dummy.end)


  def firstOccursIn(self, phage: List[str]):
    """
    Does the first interval of this FusedInterval occur in the given phage?

    note:: The child covered by this FusedInterval is compared to to the given
           phage. This way, wildcards in the child are taken into account.
    
    Parameters
    ----------
    phage : List[str]
        the phage to test
    """
    return self.occursIn(phage, self.first)


  def secondOccursIn(self, phage: List[str]):
    """
    Does the second interval of this FusedInterval occur in the given phage?

    note:: The child covered by this FusedInterval is compared to to the given
           phage. This way, wildcards in the child are taken into account.
    
    Parameters
    ----------
    phage : List[str]
        the phage to test
    """
    return self.occursIn(phage, self.second)


  def occursIn(self, phage: List[str], inter: Interval):
    """
    Does the given interval occur in the given phage?

    note:: The child covered by this FusedInterval is compared to to the given
           phage. This way, wildcards in the child are taken into account.
    
    Parameters
    ----------
    phage : List[str]
        the phage to test
    """
    for i in inter.phageIndexIter():
      if self.child[i] != WILDCARD and self.child[i] != phage[i]:
        return False

    return True


  def rightFacing(self) -> bool:
    """
    Return True if this FI is right-facing. In other words, the first interval
    starts before the second interval in the circular ordering.

    note:: assumes the first interval overlaps the second on the right.
    """
      #Test if the second interval is right of the breakinterval.
    return self.second.coversIndex(self.end_bi % self.wraplen)

  def setEssentialIntervalsCoversAll(self) -> None:
    """
    Set the essential intervals from the non-intersecting parts of self,
    which must be a FusedInterval that covers an entire phage.
    """
    int1 = self.first.getOverlapRight(self.second)
    int2 = self.second.getOverlapRight(self.first)
    assert int1[0] > 0 and int2[0] > 0, 'expected overlap on both ends'

    start, end = int2[1]%self.wraplen, int1[0]
    if start > end:                 #end wraps
      end = end + self.wraplen
    self.first.start_essential = start
    self.first.end_essential = end
    assert(self.first.start_essential <= self.first.end_essential)
    #print(f'  ({self.first.start_essential}, {self.first.end_essential})')

    start, end = int1[1]%self.wraplen, int2[0]
    if start > end:                 #end wraps
      end = end + self.wraplen
    self.second.start_essential = start
    self.second.end_essential = end
    assert(self.second.start_essential <= self.second.end_essential)
    #print(f'  ({self.second.start_essential}, {self.second.end_essential})')
    
    
  def getSecondEssentialPair(self):
    """
    Get the second interval's start_essential and end_essential. This could
    be wrapped if this FI wraps and the second essential interval starts
    after the wrap:

        ------|---|                     --------------
              |---|-----*******-
    """
    if self.second.start_essential < self.first.start:
      return (self.second.start_essential + self.wraplen,
              self.second.end_essential + self.wraplen)

    return self.second.start_essential, self.second.end_essential


  def getNonBIFirst(self, phage: Phage) -> Interval:
    """
    Return the part of self.first that isn't covered by self.second.
    In other words, its the part that is not in the breakinterval.
    
    Parameters
    ----------
    phage : Phage
        The phage sequence to associate with the interval
    
    Returns
    -------
    Interval
        An sub Interval of fusedint.first
    """
    assert(type(phage) == Phage)
  
    end = self.second.start
    if self.second.start < self.first.start:
      end = self.second.start + self.wraplen
  
    return Interval(self.first.start, end, phage, phage.name, self.wraplen)


  def getNonBISecond(self, phage: Phage) -> Interval:
    """
    Return the part of self.second that isn't covered by self.first.
    In other words, its the part that is not in the breakinterval.

    Parameters
    ----------
    phage : Phage
        The phage sequence to associate with the interval

    Returns
    -------
    Interval
        An sub Interval of self.second
    """
    assert(type(phage) == Phage)

    start = self.first.end
    if start >= self.wraplen:
      start = self.first.end - self.wraplen

    return Interval(start, self.second.end, phage, phage.name, self.wraplen)


  #def anchorInterval(self):
  #  """
  #  Return the inverval over which we see anchors.
  #  """
  #  end = self.end_bi
  #  if pythonindexed:
  #    end -= 1

  #  return self.start_bi-1, self.start_bi

  def pnamePair(self) -> Tuple[str, str]:
    return self.first.phagename, self.second.phagename

  def __len__(self):
    return self.end - self.start

  def __str__(self):
    return f'({self.start}, {self.end})'

  def __repr__(self):
    return str(self)
    #return f'{self}{self.first.phagename}{self.second.phagename}{self.childname}'

  def __lt__(self, other: 'FusedInterval'):
    if self.start == other.start:
      return self.end < other.end

    return self.start < other.start

  def __eq__(self, other):
    if other == None or isinstance(other, NullFusedInterval):
      return False
    if not isinstance(other, FusedInterval):
      raise NotImplementedError(f'Expected FusedInterval, not {type(other)}')

    return(self.start == other.start and self.end == other.end and
           self.start_bi == other.start_bi and self.end_bi == self.end_bi and
           self.childname == other.childname and
           self.first.phagename == other.first.phagename and
           self.second.phagename == other.second.phagename)

  def __le__(self, other):
    return self < other or self == other

  def __hash__(self):
    return hash((self.start, self.end, self.start_bi, self.end_bi,
                 self.first.phagename, self.second.phagename, self.childname))
   

class NullFusedInterval(FusedInterval):
  def __init__(self):
    self.start = self.end = -1

  def __eq__(self, other):
    return isinstance(other, NullFusedInterval)

NULL_FI = NullFusedInterval()


#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
class BreakpointClass:
  """
  This holds a set of mandatory breakpoints (accessed as
  `FusedInterval.essential`) that all share the same breakpoint intervals and
  variants within that interval.

  Attributes
  ----------
  bi : PAIR
    the breakpoint interval indices that distinguish this class
  slice : Tuple[str]
    the breakpoint interval variants that distinguish this class
  fiset : Set[FusedInterval]
    the set of FIs that represent the breakpoint intevals (essential)
  """
  def __init__(self):
    self.bi: Tuple[int, int] = (-1, -1)
    self.slice = ()
    self.fiset: Set[FusedInterval] = set()
  

  def add(self, fi: FusedInterval, bi: PAIR, slice: Tuple[str]):
    """
    Add the given `fi` to this class. If `self.bi` and `self.slice` are not
    set then set them, otherwise confirm that they match the given ones.

    Parameters
    ----------
    fi : FusedInterval
        [description]
    bi : PAIR
        [description]
    slice : Tuple[str]
        [description]
    """
    self.fiset.add(fi)
    assert self.bi[0] < 0 or self.bi == bi
    self.bi = bi
    assert not self.slice or self.slice == slice, f'{self.slice} == {slice}'
    self.slice = slice


  def getFIsWithParents(self, p1: Phage, p2: Phage) -> List[FusedInterval]:
    """
    Find the FusedIntervals that have `p1` as their first parent and `p2` as
    their second parent.
    """
    retlist: List[FusedInterval] = []
    for fi in self.fiset:
      if fi.first.phage == p1 and fi.second.phage == p2:
        retlist.append(fi)
  
    return retlist
  

  def __str__(self):
    retstr = ''
    for fi in self.fiset:
      retstr += fi.getAlignStrTwoLine()
    return retstr


#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
#class FusionSet:
#  """
#  Represents a set of FusedIntervals that can all be created with the same
#  recombination.
#
#  Attributes
#  ----------
#  representative: FusedInterval
#    the largest interval in childrenset
#
#  childrenset: Set(FusedInterval)
#    the set of FusedIntervals that can be satisfied with a single recombination
#  """
#
#  #FusionSet
#  def __init__(self, fusedint: FusedInterval):
#    """
#    Parameters
#    ----------
#    fusedint: FusedInterval
#      the lone element of this FusionSet.
#    """
#    self.representative: FusedInterval = fusedint
#    self.childrenset: Set[FusedInterval] = set([fusedint])
#    self.parents: Tuple[str] = (fusedint.first.parentname,
#                                fusedint.second.parentname)
#
#  def merge(self, other: 'FusionSet'):
#    raise(NotImplementedError)
#
#  def isAbsorbable(self, other: 'FusionSet'):
#    """
#    Test if the given FusionSet can be merged into this one.
#
#    A FusedInterval can be absorbed if it has the same parents in the same
#    order as this representative and:
#
#      1) the interval is contained in this one, or
#      2) the sibling is contained in this one.
#    
#    Parameters
#    ----------
#    other : FusionSet
#        the interval that would be merged with this one.
#    """
#    if self.parents != other.parents:
#      return False
#
#    if self.representative.contains(other.representative):
#      print(f'{self.representative} contains {other.representative}')
#
#    raise(NotImplementedError)

#===============================================================================

def orderIntervals(i1: Interval, i2: Interval) -> Tuple[Interval, Interval]:
  """
  Given two overlapping intervals, return them in the proper (circular)
  ordering.
  
  Parameters
  ----------
  i1 : Interval
      first interval
  i2 : Interval
      second interval
  
  Returns
  -------
  Tuple[Interval, Interval]
      i1 and i2 ordered in their circular ordering, or None if they don't touch
  """
  if i1 <= i2:
    if i2.wraps(True) and not i1.wraps() and not i1.end >= i2.start:
      return i2, i1        #i2 wraps and is the first
    else:
      assert(i1.end >= i2.start)
      return i1, i2

  else:
    if i1.wraps(True) and not i2.wraps() and not i2.end >= i1.start:
      return i1, i2        #i1 wraps and is the first
    else:
      assert(i2.end >= i1.start)
      return i2, i1


def getAlignList(iobj: Union[Interval, FusedInterval],
                 complement=False) -> List[str]:
    """
    Given an Interval or FusedInterval, return a list with the characters
    of the interval in the proper location.

    Parameters
    ----------
    iobj: Interval or FusedInterval
      the interval to convert

    complement: bool
      print the complement slice instead of the normal slice (if FusedInterval)
    """
    if complement and isinstance(iobj, FusedInterval):
      slc = iobj.c
    else:
      slc = iobj.s

    for c in slc:
      assert(len(c) == 1)            #Single character variants only!

    e = [EMPTY_CHAR]                 #The empty character
    if iobj.wraplen >= iobj.end:     #The interval doesn't wrap
      return e*iobj.start + slc + e*(iobj.wraplen - iobj.end)
    
      #The interval wraps
    leftsize = iobj.end - iobj.wraplen
    chopspot = iobj.wraplen - iobj.start
    return slc[chopspot:] + e*(iobj.start-leftsize) + slc[:chopspot]


def getAlignStr(iobj, complement=False) -> str:
    """
    Given an Interval or FusedInterval, return a string with the characters
    of the interval in the proper location.

    Parameters
    ----------
    iobj: Interval or FusedInterval
      the interval to convert

    complement: bool
      print the complement slice instead of the normal slice (if FusedInterval)
    """
    return ''.join(getAlignList(iobj, complement))


def coversAllIndicesPair(i1: Interval, i2: Interval):
  """
  Does this pair of intervals cover all indices?
  
  Parameters
  ----------
  i1 : Interval
      first interval
  i2 : Interval
      second interval
  """
  for i in range(i1.wraplen):
    if not i1.coversIndex(i) and not i2.coversIndex(i):
      return False

  return True

def isDummyName(name: str) -> bool:
  """
  Return True if the name starts with the DUMMY character.
  
  Parameters
  ----------
  name : str
      the parent name
  """
  return name[0] == DUMMY

def overlapPairs(p: PAIR, q: PAIR, wraplen: int) -> Optional[PAIR]:
  """
  Does this interval overlap the other one?

  note:: python indexed, circularly indexed
  
  Parameters
  ----------
  p : PAIR
      interval pair
  q : PAIR
      other interval pair
  wraplen : int
      the index where the intervals start to overlap
  
  Returns
  -------
  PAIR
      The interval over which the two intervals overlap, or False if they don't
  """
  lo = leftOverlaps(p, q, wraplen)
  if lo:
    return lo

  lo = leftOverlaps(q, p, wraplen)
  return lo


def leftOverlaps(p: PAIR, q: PAIR, wraplen: int) -> Optional[PAIR]:
  """
  Does this interval overlap to the left of the other one?
  
  Examples::

    |------|          or        |------|
      |------|                    |---|
  
  Parameters
  ----------
  other : FusedInterval
      the other FI
  
  Returns
  -------
  PAIR
      the overlapping interval (python indexed)
  """
  if p[1] > wraplen and q[1] <= wraplen:         #p  ------------|       |----
    if p[0] > q[0] and p[1] % wraplen > q[0]:    #q   |--| or |----|
      return (q[0], min(p[1] % wraplen, q[1]))   

    elif p[0] <= q[0]:                           #q                       |--|
      return (q[0], q[1])                        #q  --|     or           |---

  if p[0] <= q[0] and p[1] > q[0]:
    return (q[0], min(p[1], q[1]))

  return None
