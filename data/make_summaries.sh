#!/usr/bin/bash
snakemake $@ -j 7 \
2022/kupczok/timeseries1_output/timeseries1_1_summary.txt \
2022/kupczok/timeseries1_output/timeseries1_2_summary.txt \
2022/kupczok/timeseries2_output/timeseries2_1_summary.txt \
2022/kupczok/timeseries2_output/timeseries2_2_summary.txt \
2022/kupczok/timeseries3_output/timeseries3_1_summary.txt \
2022/kupczok/timeseries3_output/timeseries3_2_summary.txt \
2022/kupczok/timeseries4_output/timeseries4_1_summary.txt \
2022/kupczok/timeseries4_output/timeseries4_2_summary.txt \
2022/kupczok/timeseries5_output/timeseries5_1_summary.txt \
2022/kupczok/timeseries5_output/timeseries5_2_summary.txt \
2022/kupczok/timeseries6_output/timeseries6_1_summary.txt \
2022/kupczok/timeseries6_output/timeseries6_2_summary.txt \
2022/france_output/france_15_1_summary.txt \
2022/france_output/france_15_2_summary.txt \
2022/irish_output/irish_18_1_summary.txt \
2022/irish_output/irish_18_2_summary.txt \
2022/irish2_output/irish2_x_1_summary.txt \
2022/irish2_output/irish2_x_2_summary.txt \
2022/murphy_output/lacto_factories_1_summary.txt \
2022/murphy_output/lacto_factories_2_summary.txt \
2022/murphy1_output/murphy1_15_1_summary.txt \
2022/murphy1_output/murphy1_15_2_summary.txt \
2022/murphy3_output/murphy3_16_1_summary.txt \
2022/murphy3_output/murphy3_16_2_summary.txt \
2022/murphy3b_output/murphy3b_16_1_summary.txt \
2022/murphy3b_output/murphy3b_16_2_summary.txt
