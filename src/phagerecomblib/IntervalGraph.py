# Copyright 2019 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
The interval graph classes.
"""
import re
from collections import defaultdict as defdict
from collections import deque
from itertools import chain
from itertools import tee
from sre_constants import IN
from typing import List
from typing import Dict
from typing import Iterable
from typing import Tuple
from typing import Deque
from typing import Optional
import networkx as nx
import matplotlib.pyplot as plt

from .Intervals import Interval
from .Intervals import FusedInterval


#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
class INode:
  """
  An Interval Graph node.

  Attributes
  ----------
  parentname : str
    the name of the parent for this variant

  start : int
    the start position of the parent

  end : int
    the end position of the parent

  s : str
    the slice that this node represents
  """

  #INode
  def __init__(self, parentname: str, slc: str, start: int, end: int=None):
    """
    Make an INode.
    
    Parameters
    ----------
    parentname : str
        name of parent
    slc : str
      the slice that this node represents
    start : int
        start index
    end : int, optional
        end index, will be start+1 if not specified, by default None
    """
    self.parentname = parentname
    self.start = start
    self.s = slc

    if end:
      self.end = end
    else:
      self.end = start+1

  def __str__(self):
    if self.end - self.start == 1:
      return f'{self.parentname}: {self.start}\n{self.s}'
    else:
      return f'{self.parentname}: [{self.start}, {self.end})\n{self.s}'

  def __repr__(self):
    return str(self)




#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
class IntervalGraph(nx.DiGraph):
  """
  An Interval Graph.

  Attributes
  ----------
  phagelen : int
    the end position of the parent

  fusedints : List[FusedInterval]
    the FusedIntervals that this graph represents

  parentTOlocTOnode : Dict[List[INode]]
    hold one node for each position of a parent phage that is in an interval
  """

  #Intervalgraph
  def __init__(self, fusedints: List[FusedInterval], *args, **kwargs) -> None:
    super().__init__(self, *args, **kwargs)

    self.phagelen = fusedints[0].wraplen
    self.fusedints = fusedints
    self.parentTOlocTOnode: Dict[str, List[Optional[INode]]] = \
      defdict(lambda: [None]*self.phagelen)

      #Make a node for each spot that is covered by a parent:
    for fi in fusedints:
      self._addFusedIntervalNodesEdges(fi)

      #Add connectivity for the transitions between the breakintervals:
    for fi in fusedints:
      self._addBreakIntervalEdges(fi)


  def _addFusedIntervalNodesEdges(self, fusedint: FusedInterval):
    """
    Make a node (if not there already) for each spot that is covered by these
    two parents. Make an edge between each covered spot in an interval.
    
    Parameters
    ----------
    fusedint : FusedInterval
        the parent to create nodes for
    """
    lastf = None   #The last index visited for the first interval.
    lasts = None   #The last index visited for the second interval.
    for i in fusedint.phageIndexIter():
      if fusedint.first.coversIndex(i):
        newnode = self._addNodeForParent(fusedint.first, i)
        if lastf:
          self.addEdge(lastf, newnode, fusedint.childname)
        lastf = newnode

      if fusedint.second.coversIndex(i):
        newnode = self._addNodeForParent(fusedint.second, i)
        if lasts:
          self.addEdge(lasts, newnode, fusedint.childname)
        lasts = newnode


  def _addNodeForParent(self, interval: Interval, i: int) -> INode:
    if not self.parentTOlocTOnode[interval.parentname][i]:
      newnode = INode(interval.parentname, interval[i], i)
      self.parentTOlocTOnode[interval.parentname][i] = newnode
      self.add_node(newnode)
      return newnode

    oldnode = self.parentTOlocTOnode[interval.parentname][i]
    assert oldnode
    return oldnode


  def _addBreakIntervalEdges(self, fusedint: FusedInterval):
    """
    Add all the edges for the breakinterval in this FusedInterval.
    
    Parameters
    ----------
    fusedint : FusedInterval
        the interval
    """
    #print(f'____')
    #print(f'({fusedint.start_bi},{fusedint.end_bi}) {fusedint.s}')
    pTlTn = self.parentTOlocTOnode
    for i in chain([fusedint.start_bi-1], fusedint.phageIndexBIIter()):
      #print(f'{fusedint.getAlignStr()}')
      #print(f'{fusedint.first.getAlignStr()}')
      #print(f'{fusedint.second.getAlignStr()}')
      #print(f'{fusedint.first[i]} -> {fusedint.second[(i+1)%fusedint.wraplen]}')
      #print(f'{fusedint.second[i]} -> {fusedint.first[(i+1)%fusedint.wraplen]}')
      u = pTlTn[fusedint.first.parentname][i]
      v = pTlTn[fusedint.second.parentname][(i+1)%fusedint.wraplen]
      assert u and v
      self.addEdge(u, v, fusedint.childname)


  def addEdge(self, u: INode, v: INode, name: str) -> None:
    """
    Add an edge to the graph with the given name label. If it exists already,
    then add the name to the label set.
    
    Parameters
    ----------
    u : INode
        from node
    v : INode
        to node
    name : str
        the phage name to add
    """
    if u in self and v in self[u]:
      self[u][v]['labelset'].add(name)
      self[u][v]['label'] = f"({' '.join(self[u][v]['labelset'])})"
    else:
      self.add_edge(u, v, labelset=set([name]), label=f'({name})')


  def _addAllGenomeEdges(self):
    """
    Add all the edges for the genomes.

    warn:: doesn't update labeld properly
    
    Parameters
    ----------
    fusedint : FusedInterval
        the interval
    """
    for nodelist in self.parentTOlocTOnode.values():
      nonnullnodes = [node for node in nodelist if node]
      for i in range(len(nonnullnodes) - 1):
        self.add_edge(nonnullnodes[i], nonnullnodes[i+1])

      self.add_edge(nonnullnodes[-1], nonnullnodes[0])


  def drawGraph(self, filename:str) -> None:
    """
    Draw the graph to the given filename.
    
    Parameters
    ----------
    filename : str
        the file to write the graph to
    """
     #Set node positions:
    hscale = 1.8
    vscale = 1.5
    for rownum, name in enumerate(self._getOrderedGenomes()):
      for columnnum, node in enumerate(self.parentTOlocTOnode[name]):
        if node:
          self.nodes[node]['pos'] = f'{columnnum*hscale}, {rownum*vscale}!'

    agraph = nx.nx_agraph.to_agraph(self)
    agraph.graph_attr['splines'] = True

    # #Set node positions:
    #genomeTOnodes = defdict(list)
    #genomeTOleftmost = {}
    #namere = re.compile(r'(\w): \[(\d+), (\d+)\)')
    #for node in agraph.nodes():
    #  #print(f'node: {node.name}')
    #  #print(f'node: {node.name}\n\tcolumn: {node.attr["column"]}')
    #  m = namere.match(node.name)
    #  if not m:
    #    raise(Exception(f'unrecognized phage name string "{node.name}"'))

    #  name = m.group(1)
    #  start = int(m.group(2))
    #  genomeTOnodes[name].append(node)

    #  if name not in genomeTOleftmost or genomeTOleftmost[name][0] > start:
    #    genomeTOleftmost[name] = (start, node)

    #  #print(f'{dir(node)}')

    #  #Group the node from the same genome so that they're in the same line:
    #for name, nodes in genomeTOnodes.items():
    #  agraph.add_subgraph(nodes, name=f'{name}_subgraph', rank='same')

    #  #Add invisible edges so that the genomes appear on different lines:
    #last = None
    #for _, node in genomeTOleftmost.values():
    #  if last:
    #    agraph.add_edge(last, node, style='invis')
    #  last = node

    #agraph.graph_attr.update(ranksep='1')

    #G = nx.nx_agraph.from_agraph(agraph)
    agraph.draw(filename, prog='neato')


  def _getOrderedGenomes(self) -> Deque[str]:
    """
    Order the genomes in a sensible way (most connected come adjacent in the
    ordering).
    
    Returns
    -------
    List[str]
        the list of genome names, each occurring once
    """
      #Find the most connected phage:
    nameTOnameTOcount: Dict[str, Dict[str, int]] = defdict(lambda: defdict(int))
    nameTOcount = defdict(int)
    for fi in self.fusedints:
      nameTOnameTOcount[fi.first.parentname][fi.second.parentname] += 1
      nameTOnameTOcount[fi.second.parentname][fi.first.parentname] += 1
      nameTOcount[fi.first.parentname] += 1
      nameTOcount[fi.second.parentname] += 1
    

      #Build the linear ordering greedily:
    mostconnected = self._sortedNameToCount(nameTOcount)[0][0]

    ordering = deque()       #the ordering we're building
    countsbetween = deque()  #connections between ordering[i] and ordering[i+1]

    ordering.append(mostconnected)
    left = mostconnected
    right = mostconnected
    while left or right:             #While we have neighbors:
      if left:
        neighbor = self._mostConnectedNeighbor(left, nameTOnameTOcount)
        if neighbor:
          ordering.appendleft(neighbor)
          countsbetween.appendleft(nameTOnameTOcount[left][neighbor])
        nameTOnameTOcount.pop(left)  #remove name from consideration
        left = neighbor

      if right:
        neighbor = None
        if nameTOnameTOcount:
          neighbor = self._mostConnectedNeighbor(right, nameTOnameTOcount)
          if neighbor:
            ordering.append(neighbor)
            countsbetween.append(nameTOnameTOcount[right][neighbor])
        nameTOnameTOcount.pop(right) #remove name from consideration
        right = neighbor

      #Insert remaining phages between a pair that maximizes connections:
    for name in nameTOnameTOcount:
      best = 0
      insloc = 0         #Insert before this element.
      for i, (n1, n2) in enumerate(pairwise(nameTOnameTOcount[name])):
        score = nameTOnameTOcount[name][n1] + nameTOnameTOcount[name][n2]
        if score > best:
          best = score
          insloc = i

      ordering.insert(insloc, name)

    return ordering



  def _mostConnectedNeighbor(self, name:str,
                             nameTOnameTOcount: Dict[str, Dict[str, int]]) -> str:
    """
    Get the neighbor that is most connected to the given name, and is still in
    the dictionary.
    
    Parameters
    ----------
    nameTOnameTOcount : Dict[str, Dict[str, int]]
        the number of FusedIntervals for a pair of names
    
    Returns
    -------
    str
        the name of the most connected
    """
    for name, _ in self._sortedNameToCount(nameTOnameTOcount[name]):
      if name in nameTOnameTOcount:
        return name

    assert False, 'name must be in nameTOnameTOcount'


  def _sortedNameToCount(self, nameTOcount: Dict[str, int], reverse=True):
    return sorted(nameTOcount.items(), key=lambda nc: nc[1], reverse=reverse)

  def showGraph(self):
    pos = nx.nx_agraph.graphviz_layout(self, prog='dot')
    nx.draw(self, pos, with_labels=True)
    plt.show()
    

  def printNodes(self):
    print(f'Nodes:\n' + '\n'.join(map(str, self.nodes)))

################################################################################

def pairwise(it: Iterable[str]) -> Iterable[Tuple[str, str]]:
  it1, it2 = tee(it)
  next(it2, None)
  return zip(it1, it2)

